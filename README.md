ZOIDS MMORPG
===========

Website Repo for [the site](http://shadowys.github.io/Zoids-MMORPG/)

[Come and help out with the dev team by reporting bugs and giving suggestions!](http://zoidsmmorpg.freeforums.org/)

[Donate for a mighty cause! All proceeds go to game development.](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=4Z8QCKS4GB73A)

We have launched the game! It's in the [Game folder](Game).